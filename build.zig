const Build = @import("std").Build;

pub fn build(b: *Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    // Deps
    const zg = b.dependency("zg", .{});

    const cow_list = b.dependency("cow_list", .{
        .target = target,
        .optimize = optimize,
    });

    // Module
    _ = b.addModule("zigstr", .{
        .root_source_file = b.path("src/Zigstr.zig"),
        .imports = &.{
            .{ .name = "cow_list", .module = cow_list.module("cow_list") },
            .{ .name = "code_point", .module = zg.module("code_point") },
            .{ .name = "grapheme", .module = zg.module("grapheme") },
            .{ .name = "PropsData", .module = zg.module("PropsData") },
            .{ .name = "CaseData", .module = zg.module("CaseData") },
        },
    });

    const main_tests = b.addTest(.{
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
    });
    main_tests.root_module.addImport("cow_list", cow_list.module("cow_list"));
    main_tests.root_module.addImport("code_point", zg.module("code_point"));
    main_tests.root_module.addImport("grapheme", zg.module("grapheme"));
    main_tests.root_module.addImport("PropsData", zg.module("PropsData"));
    main_tests.root_module.addImport("CaseData", zg.module("CaseData"));

    const run_tests = b.addRunArtifact(main_tests);
    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(&run_tests.step);
}
